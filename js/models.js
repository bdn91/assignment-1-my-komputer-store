export class Bank {
    constructor(balance = 0, loan = 0, loanAllowed = true) {
        this.balance = balance
        this.loan = loan
        this.loanAllowed = loanAllowed
    }
}

export class Work {
    constructor(balance = 0) {
        this.balance = balance
    }
}
