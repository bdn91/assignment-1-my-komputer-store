import productsSeed from './productsSeed.js'
import { Bank, Work } from './models.js'
import { BankService, WorkService } from './services.js'

const products = productsSeed
const bank = new Bank()
const work = new Work()
const workService = new WorkService()
const bankService = new BankService()

const elTransferButton = document.getElementById('btn-transfer')
const elWorkButton = document.getElementById('btn-work')
const elPayLoanButton = document.getElementById('btn-pay-loan')
const elProductBuyButton = document.getElementById('btn-buy-product')
const elApplyLoanButton = document.getElementById('btn-apply-loan')
const elWorkBalance = document.getElementById('work-balance-value')
const elBankBalance = document.getElementById('bank-balance-value')
const elOutstandingLoan = document.getElementById('outstanding-loan-value')
const elPayLoanColumn = document.getElementById('col-pay-loan')
const elSelectProductList = document.getElementById('select-product-list')
const elFeaturesList = document.getElementById('features-list')
const elProductImage = document.getElementById('product-image')
const elProductDescription = document.getElementById('product-description')
const elProductPrice = document.getElementById('product-price')
const elProductCartName = document.getElementById('product-cart-name')

elProductBuyButton.onclick = () => {
    bankService.buyProduct(bank, products[elSelectProductList.value])
    render()
}

elSelectProductList.onchange = () => {
    renderProductFeatures()
    renderProductCart()
}

elPayLoanButton.onclick = () => {
    bankService.payLoan(bank, work)
    render()
}

elApplyLoanButton.onclick = () => {
    bankService.applyLoan(bank)
    render()
}

elWorkButton.onclick = () => {
    workService.increaseBalance(work)
    render()
}

elTransferButton.onclick = () => {
    workService.transfer(work, bank)
    render()
}

const render = () => {
    elBankBalance.innerText = bank.balance
    elWorkBalance.innerText = work.balance
    elOutstandingLoan.innerText = bank.loan

    if (bank.loan > 0) {
        elPayLoanColumn.classList.remove('d-none')
    } else {
        elPayLoanColumn.classList.add('d-none')
    }

    if (work.balance > 0) {
        elTransferButton.disabled = false
        elPayLoanButton.disabled = false
    } else {
        elTransferButton.disabled = true
        elPayLoanButton.disabled = true
    }
}

const renderDropDown = () => {
    products.forEach(product => {
        elSelectProductList.innerHTML += `<option value="${product.id}">${product.name}</option>`
    })
}

const renderProductFeatures = () => {
    const selectedIndex = parseInt(elSelectProductList.value)
    const selectedFeatures = products[selectedIndex].features
    elFeaturesList.innerHTML = ''
    selectedFeatures.forEach(feature => {
        elFeaturesList.innerHTML += `<p>${feature}</p>`
    })
}

const renderProductCart = () => {
    const selectedIndex = parseInt(elSelectProductList.value)
    const selectedProduct = products[selectedIndex]
    elProductImage.src = ''
    elProductCartName.innerText = ''
    elProductDescription.innerText = ''
    elProductPrice.innerText = ''
    elProductCartName.innerText = selectedProduct.name
    elProductDescription.innerText = selectedProduct.description
    elProductImage.src = selectedProduct.imageSource
    elProductPrice.innerText = selectedProduct.price + ' KR'
}

renderDropDown()
renderProductFeatures()
renderProductCart()
