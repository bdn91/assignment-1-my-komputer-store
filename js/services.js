export class WorkService {
    constructor() {
        this.SALARY = 100
        this.LOAN_TRANSFER_RATE = 0.1
    }

    transfer(work, bank) {
        if (bank.loan > 0) {
            bank.loan -= work.balance * this.LOAN_TRANSFER_RATE
            bank.balance += work.balance * (1 - this.LOAN_TRANSFER_RATE)

            if (bank.loan < 0) {
                bank.balance -= bank.loan
                bank.loan = 0
            }
        } else {
            bank.balance += work.balance
        }
        work.balance = 0
    }

    increaseBalance(work) {
        work.balance += this.SALARY
    }
}

export class BankService {
    applyLoan(bank) {
        if (bank.loan > 0) {
            alert('You must pay the outstanding loan before getting a new loan!')
            return
        }
        if (!bank.loanAllowed) {
            alert('Loan is already given, buy something first')
            return
        }

        const amount = prompt('Please enter the amount of kroner you want to borrow', 0)

        if (isNaN(parseInt(amount)) || amount <= 0) {
            if (amount !== null) {
                alert('You entered a invalid amount')
            }
            return
        }

        if (amount >= bank.balance * 2) {
            alert('You cannot borrow more than the double of the amount you already have in the bank')
            return
        }
        bank.balance += parseInt(amount)
        bank.loan = amount
        bank.loanAllowed = false
    }

    payLoan(bank, work) {
        bank.loan -= work.balance
        if (bank.loan < 0) {
            alert('You\'re loan has been paid and the rest of you payment has been transfered to your bank balance')
            bank.balance -= bank.loan
            bank.loan = 0
        }
        work.balance = 0
    }

    buyProduct(bank, product) {
        if (product.price > bank.balance) {
            alert('You don\'t have enough money to buy this product. Apply for a loan or work more')
        } else {
            alert(`Congratulations with your new product: ${product.name}`)
            bank.balance -= product.price
            bank.loanAllowed = true
        }
    }
}
