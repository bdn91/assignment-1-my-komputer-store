export default [
    {
        id: 0,
        name: 'MacBook Pro 13”',
        price: '3000',
        description: `13-inch MacBook Pro is completely transformed by the M1 chip. 
        Up to 2.8x faster CPU. Up to 5x faster graphics. The longest battery life ever in a Mac.`,
        imageSource: 'img/mac.png',
        features: [
            'Intel i9',
            '16 GB Ram',
            '1 TB SSD'
        ]
    },
    {
        id: 1,
        name: 'Lenovo ThinkPad 15”',
        price: '2500',
        description: `Propel your business forward with the 15.6" Lenovo ThinkBook 15 Gen 2 (Intel). 
        This powerful laptop sports up to a cutting-edge 11th Gen Intel® Core™ processor and offers a range of robust 
        storage and memory options. Smart features include optimized conferencing for seamless remote work.`,
        imageSource: 'img/lenovo.jpg',
        features: [
            'Intel i7',
            '32 GB Ram',
            '512 GB SSD'
        ]
    },
    {
        id: 2,
        name: 'Dell XPS 13”',
        price: '1000',
        description: `Stunning inside and out the remastered 13-inch now features 10th Gen Intel® Core™ processors, 
        an innovative HD webcam located in the top of the InfinityEdge display and next generation Dell Cinema.`,
        imageSource: 'img/dell.jpeg',
        features: [
            'Intel i5',
            '16 GB Ram',
            '256 GB SSD'
        ]
    },
    {
        id: 3,
        name: 'Razor Blade 13”',
        price: '2000',
        description: `Enter the next evolution of ultra-mobility and extreme performance. Utilizing the latest and greatest 
        Ryzen™ 9 processor Zen 3 at 28W and a powerful GeForce® GTX 1650 Ti, you’ll rip through games and content 
        creation tasks. Featuring the world’s fastest 13.3” 120Hz Full HD display for buttery smooth visuals or an all new 
        OLED touch display - the Razer Blade Stealth 13 combines portability and style, with performance that’s perfect for gaming 
        and productivity.`,
        imageSource: 'img/razer.jpg',
        features: [
            'Ryzen 7',
            '16 GB Ram',
            '512 GB SSD'
        ]
    }
]
